<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents** 

- [About Me](#about-me)
- [Projects I have worked on](#projects-i-have-worked-on)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## About Me

Hello there!

I'm university graduate from KAMK, majored in game programming. 

Here is my small portfolio of stuff i have worked on during last few years.

I'm always interested to try out new things and challenge myself, which maybe can be seen various platforms i have worked on. Going forward, I try to expand on various hardwares too.

I also am interested in different parts of game development, helping me to understand and communicate different aspects of the game development.

Under i have list projects i have worked on. There is also some personal projects and code snippets in repo files.


## Projects I have worked on

[Ninjas with Guns]
- Arena shooter game made with Roblox Engine. I worked on shooting and things related to it.
- Trailer: https://youtu.be/7zvT-NwwaWs
- Role: Programmer
- Duration: 3 months
- Note: Some members continued on project later and its playable on Roblox.

[Kyrber]
- Mobile action game made with Godot Engine. I worked on leading the project and designing game. Game was unfinished.
- Trailer: https://www.youtube.com/watch?v=KZhpFNRzvQw
- Role: Team lead/Designer
- Duration: About 2 and half months worth of work days

[Eternity Entrance]
- Small puzzle game made with Unreal Engine 4. I designed and worked on player and various interaction/general features.
- Trailer: https://www.youtube.com/watch?v=QMgs0GKIjWc
- Role: Programmer
- Duration: About 2 and half months worth of work days
- Note: Playable on itch.io: https://eternity-entrance.itch.io/eternity-entrance

[Mëtzlson]
- Mecha arena shooter demo made with Unreal Engine 4. I worked on leading the project.
- Trailer: https://www.youtube.com/watch?v=OsRLZZNYf6c
- Role: Creative Director
- Duration: About 2 and half months worth of work days
- Note: Playable on gamejolt.com: https://gamejolt.com/games/metzlson/566117

**Other**

Internship in USA based startup studio Blue Diamond studios. I worked remotely in international team as game programmer. Worked on UE4, mostly on AI and little bit with Niagara system. Also participated on design and bit on project management.

I have also worked on other platforms on small projects or jams. These include:
- Unity
- Urho3D
- Excel

