﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Diagnostics;

namespace DungeonCrawler
{
    public class Map
    {
        #region pinvoke ReadConsoleOutputCharacter
        [DllImport("kernel32", SetLastError = true)]
        static extern IntPtr GetStdHandle(int num);
        [DllImport("kernel32", SetLastError = true, CharSet = CharSet.Ansi)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool ReadConsoleOutputCharacterA(

        IntPtr hStdout,   // result of 'GetStdHandle(-11)'
        out byte ch,      // A̲N̲S̲I̲ character result
        uint c_in,        // (set to '1')
        uint coord_XY,    // screen location to read, X:loword, Y:hiword
        out uint c_out);  // (unwanted, discard)
        //https://stackoverflow.com/questions/12355378/read-from-location-on-console-c-sharp rakennettu tämän varassa
        #endregion

        private char[,] cord;
        private string CurrentMap;
        private int playerX;
        private int playerY;
        private bool PlayerSpawn = true;
        

        #region GetSetit
        public string CurrentMap1 { get => CurrentMap; set => CurrentMap = value; }
        public char[,] Cord { get => cord; set => cord = value; }
        public int playerX1 { get => playerX; set => playerX = value; }
        public int playerY1 { get => playerY; set => playerY = value; }
        public bool PlayerSpawn1 { get => PlayerSpawn; set => PlayerSpawn = value; }
        #endregion

        public void MapInit(Map M)
        {
            if (M.CurrentMap1 == null)
            {
                M.CurrentMap = "map1";
            }
            Console.WriteLine(MapAssets.Maps(M.CurrentMap));
            MapCoord(M);
            
        }

        public void DrawMap(Map M)
        {
            Console.Clear();
            int[] x = new int[M.cord.GetLength(0)];
            int[] y = new int[M.cord.GetLength(1)];
            
            if (PlayerSpawn)
            {
                for (uint i = 0; i < y.Length; i++)
                {
                    for (uint j = 0; j < x.Length; j++)
                    {
                        if (M.Cord[j, i] == 'S')//Setting up spawn position
                        {
                            playerX = Convert.ToInt32(j);
                            playerY = Convert.ToInt32(i);
                        }
                    }
                }
                Console.SetCursorPosition(playerX, playerY);
                Console.Write(MapAssets.PlayerIcon());
                M.Cord[playerX, playerY] = MapAssets.Ground();
                PlayerSpawn = false;
            }
            
            for (int i = playerY-3; i < (playerY+4); i++)
            {
                for (int j = playerX-5; j < (playerX+6); j++)
                {
                    bool edgeCheck = false;//Check for borders and playerIcon
                    char[] edges = MapAssets.Edges();
                    foreach (var item in edges)
                    {
                        if(item == M.Cord[j, i] || j == playerX & i == playerY || j > x.Length || i > y.Length|| j < 0 || i < 0)//Try to fix out of bounds proplem, prop needs cleaning
                        {
                            edgeCheck = true;
                        }
                        else if (item == M.Cord[j, i] || j==playerX & i==playerY)
                        {
                            
                        }
                    }
                    if (!edgeCheck)
                    {
                        Console.SetCursorPosition(j, i);
                        Console.Write(M.Cord[j, i]);
                    }
                    else if (j == playerX & i == playerY)
                    {
                        Console.SetCursorPosition(j, i);
                        Console.Write(MapAssets.PlayerIcon());
                    }
                }
            }
        }

        public void MapVisibility(Map M)
        {
            int VisX = playerX;
            int VisY = playerY;
            #region Clock12
            if (M.Cord[VisX,VisY]!=MapAssets.Wall())
            {
                VisY--;
                if (M.Cord[VisX, VisY] != MapAssets.Wall())
                {
                    Console.SetCursorPosition(VisX,VisY);
                    Console.Write(M.Cord[VisX,VisY]);
                    VisY--;
                    if (M.Cord[VisX, VisY] != MapAssets.Wall())
                    {
                        Console.SetCursorPosition(VisX, VisY);
                        Console.Write(M.Cord[VisX, VisY]);
                        VisY--;
                        if (M.Cord[VisX, VisY] != MapAssets.Wall())
                        {
                            Console.SetCursorPosition(VisX, VisY);
                            Console.Write(M.Cord[VisX, VisY]);
                            VisY--;
                            if (M.Cord[VisX, VisY] != MapAssets.Wall())
                            {
                                Console.SetCursorPosition(VisX, VisY);
                                Console.Write(M.Cord[VisX, VisY]);
                                VisY--;
                                if (M.Cord[VisX, VisY] != MapAssets.Wall())
                                {
                                    Console.SetCursorPosition(VisX, VisY);
                                    Console.Write(M.Cord[VisX, VisY]);
                                    VisY--;
                                    if (M.Cord[VisX, VisY] != MapAssets.Wall())
                                    {
                                        Console.SetCursorPosition(VisX, VisY);
                                        Console.Write(M.Cord[VisX, VisY]);
                                        
                                    }
                                }
                            }
                        }
                    }
                }
            }
            #endregion

            #region Clock1
            if (M.Cord[VisX, VisY] != MapAssets.Wall())
            {
                VisY--;
                VisX++;
                if (M.Cord[VisX, VisY] != MapAssets.Wall())
                {
                    Console.SetCursorPosition(VisX, VisY);
                    Console.Write(M.Cord[VisX, VisY]);
                    VisY--;
                    VisX++;
                    if (M.Cord[VisX, VisY] != MapAssets.Wall())
                    {
                        Console.SetCursorPosition(VisX, VisY);
                        Console.Write(M.Cord[VisX, VisY]);
                        VisY--;
                        VisX++;
                        if (M.Cord[VisX, VisY] != MapAssets.Wall())
                        {
                            Console.SetCursorPosition(VisX, VisY);
                            Console.Write(M.Cord[VisX, VisY]);
                            VisY--;
                            VisX++;
                            if (M.Cord[VisX, VisY] != MapAssets.Wall())
                            {
                                Console.SetCursorPosition(VisX, VisY);
                                Console.Write(M.Cord[VisX, VisY]);
                                VisY--;
                                VisX++;
                                if (M.Cord[VisX, VisY] != MapAssets.Wall())
                                {
                                    Console.SetCursorPosition(VisX, VisY);
                                    Console.Write(M.Cord[VisX, VisY]);
                                    VisY--;
                                    VisX++;
                                    if (M.Cord[VisX, VisY] != MapAssets.Wall())
                                    {
                                        Console.SetCursorPosition(VisX, VisY);
                                        Console.Write(M.Cord[VisX, VisY]);

                                    }
                                }
                            }
                        }
                    }
                }
            }
            #endregion

            #region Clock1
            if (M.Cord[VisX, VisY] != MapAssets.Wall())
            {
                VisY--;
                VisX++;
                if (M.Cord[VisX, VisY] != MapAssets.Wall())
                {
                    Console.SetCursorPosition(VisX, VisY);
                    Console.Write(M.Cord[VisX, VisY]);
                    VisY--;
                    VisX++;
                    if (M.Cord[VisX, VisY] != MapAssets.Wall())
                    {
                        Console.SetCursorPosition(VisX, VisY);
                        Console.Write(M.Cord[VisX, VisY]);
                        VisY--;
                        VisX++;
                        if (M.Cord[VisX, VisY] != MapAssets.Wall())
                        {
                            Console.SetCursorPosition(VisX, VisY);
                            Console.Write(M.Cord[VisX, VisY]);
                            VisY--;
                            VisX++;
                            if (M.Cord[VisX, VisY] != MapAssets.Wall())
                            {
                                Console.SetCursorPosition(VisX, VisY);
                                Console.Write(M.Cord[VisX, VisY]);
                                VisY--;
                                VisX++;
                                if (M.Cord[VisX, VisY] != MapAssets.Wall())
                                {
                                    Console.SetCursorPosition(VisX, VisY);
                                    Console.Write(M.Cord[VisX, VisY]);
                                    VisY--;
                                    VisX++;
                                    if (M.Cord[VisX, VisY] != MapAssets.Wall())
                                    {
                                        Console.SetCursorPosition(VisX, VisY);
                                        Console.Write(M.Cord[VisX, VisY]);

                                    }
                                }
                            }
                        }
                    }
                }
            }
            #endregion

            #region Clock1
            if (M.Cord[VisX, VisY] != MapAssets.Wall())
            {
                VisY--;
                VisX++;
                if (M.Cord[VisX, VisY] != MapAssets.Wall())
                {
                    Console.SetCursorPosition(VisX, VisY);
                    Console.Write(M.Cord[VisX, VisY]);
                    VisY--;
                    VisX++;
                    if (M.Cord[VisX, VisY] != MapAssets.Wall())
                    {
                        Console.SetCursorPosition(VisX, VisY);
                        Console.Write(M.Cord[VisX, VisY]);
                        VisY--;
                        VisX++;
                        if (M.Cord[VisX, VisY] != MapAssets.Wall())
                        {
                            Console.SetCursorPosition(VisX, VisY);
                            Console.Write(M.Cord[VisX, VisY]);
                            VisY--;
                            VisX++;
                            if (M.Cord[VisX, VisY] != MapAssets.Wall())
                            {
                                Console.SetCursorPosition(VisX, VisY);
                                Console.Write(M.Cord[VisX, VisY]);
                                VisY--;
                                VisX++;
                                if (M.Cord[VisX, VisY] != MapAssets.Wall())
                                {
                                    Console.SetCursorPosition(VisX, VisY);
                                    Console.Write(M.Cord[VisX, VisY]);
                                    VisY--;
                                    VisX++;
                                    if (M.Cord[VisX, VisY] != MapAssets.Wall())
                                    {
                                        Console.SetCursorPosition(VisX, VisY);
                                        Console.Write(M.Cord[VisX, VisY]);

                                    }
                                }
                            }
                        }
                    }
                }
            }
            #endregion

            #region Clock1
            if (M.Cord[VisX, VisY] != MapAssets.Wall())
            {
                VisY--;
                VisX++;
                if (M.Cord[VisX, VisY] != MapAssets.Wall())
                {
                    Console.SetCursorPosition(VisX, VisY);
                    Console.Write(M.Cord[VisX, VisY]);
                    VisY--;
                    VisX++;
                    if (M.Cord[VisX, VisY] != MapAssets.Wall())
                    {
                        Console.SetCursorPosition(VisX, VisY);
                        Console.Write(M.Cord[VisX, VisY]);
                        VisY--;
                        VisX++;
                        if (M.Cord[VisX, VisY] != MapAssets.Wall())
                        {
                            Console.SetCursorPosition(VisX, VisY);
                            Console.Write(M.Cord[VisX, VisY]);
                            VisY--;
                            VisX++;
                            if (M.Cord[VisX, VisY] != MapAssets.Wall())
                            {
                                Console.SetCursorPosition(VisX, VisY);
                                Console.Write(M.Cord[VisX, VisY]);
                                VisY--;
                                VisX++;
                                if (M.Cord[VisX, VisY] != MapAssets.Wall())
                                {
                                    Console.SetCursorPosition(VisX, VisY);
                                    Console.Write(M.Cord[VisX, VisY]);
                                    VisY--;
                                    VisX++;
                                    if (M.Cord[VisX, VisY] != MapAssets.Wall())
                                    {
                                        Console.SetCursorPosition(VisX, VisY);
                                        Console.Write(M.Cord[VisX, VisY]);

                                    }
                                }
                            }
                        }
                    }
                }
            }
            #endregion

            #region Clock1
            if (M.Cord[VisX, VisY] != MapAssets.Wall())
            {
                VisY--;
                VisX++;
                if (M.Cord[VisX, VisY] != MapAssets.Wall())
                {
                    Console.SetCursorPosition(VisX, VisY);
                    Console.Write(M.Cord[VisX, VisY]);
                    VisY--;
                    VisX++;
                    if (M.Cord[VisX, VisY] != MapAssets.Wall())
                    {
                        Console.SetCursorPosition(VisX, VisY);
                        Console.Write(M.Cord[VisX, VisY]);
                        VisY--;
                        VisX++;
                        if (M.Cord[VisX, VisY] != MapAssets.Wall())
                        {
                            Console.SetCursorPosition(VisX, VisY);
                            Console.Write(M.Cord[VisX, VisY]);
                            VisY--;
                            VisX++;
                            if (M.Cord[VisX, VisY] != MapAssets.Wall())
                            {
                                Console.SetCursorPosition(VisX, VisY);
                                Console.Write(M.Cord[VisX, VisY]);
                                VisY--;
                                VisX++;
                                if (M.Cord[VisX, VisY] != MapAssets.Wall())
                                {
                                    Console.SetCursorPosition(VisX, VisY);
                                    Console.Write(M.Cord[VisX, VisY]);
                                    VisY--;
                                    VisX++;
                                    if (M.Cord[VisX, VisY] != MapAssets.Wall())
                                    {
                                        Console.SetCursorPosition(VisX, VisY);
                                        Console.Write(M.Cord[VisX, VisY]);

                                    }
                                }
                            }
                        }
                    }
                }
            }
            #endregion

            #region Clock1
            if (M.Cord[VisX, VisY] != MapAssets.Wall())
            {
                VisY--;
                VisX++;
                if (M.Cord[VisX, VisY] != MapAssets.Wall())
                {
                    Console.SetCursorPosition(VisX, VisY);
                    Console.Write(M.Cord[VisX, VisY]);
                    VisY--;
                    VisX++;
                    if (M.Cord[VisX, VisY] != MapAssets.Wall())
                    {
                        Console.SetCursorPosition(VisX, VisY);
                        Console.Write(M.Cord[VisX, VisY]);
                        VisY--;
                        VisX++;
                        if (M.Cord[VisX, VisY] != MapAssets.Wall())
                        {
                            Console.SetCursorPosition(VisX, VisY);
                            Console.Write(M.Cord[VisX, VisY]);
                            VisY--;
                            VisX++;
                            if (M.Cord[VisX, VisY] != MapAssets.Wall())
                            {
                                Console.SetCursorPosition(VisX, VisY);
                                Console.Write(M.Cord[VisX, VisY]);
                                VisY--;
                                VisX++;
                                if (M.Cord[VisX, VisY] != MapAssets.Wall())
                                {
                                    Console.SetCursorPosition(VisX, VisY);
                                    Console.Write(M.Cord[VisX, VisY]);
                                    VisY--;
                                    VisX++;
                                    if (M.Cord[VisX, VisY] != MapAssets.Wall())
                                    {
                                        Console.SetCursorPosition(VisX, VisY);
                                        Console.Write(M.Cord[VisX, VisY]);

                                    }
                                }
                            }
                        }
                    }
                }
            }
            #endregion

            #region Clock1
            if (M.Cord[VisX, VisY] != MapAssets.Wall())
            {
                VisY--;
                VisX++;
                if (M.Cord[VisX, VisY] != MapAssets.Wall())
                {
                    Console.SetCursorPosition(VisX, VisY);
                    Console.Write(M.Cord[VisX, VisY]);
                    VisY--;
                    VisX++;
                    if (M.Cord[VisX, VisY] != MapAssets.Wall())
                    {
                        Console.SetCursorPosition(VisX, VisY);
                        Console.Write(M.Cord[VisX, VisY]);
                        VisY--;
                        VisX++;
                        if (M.Cord[VisX, VisY] != MapAssets.Wall())
                        {
                            Console.SetCursorPosition(VisX, VisY);
                            Console.Write(M.Cord[VisX, VisY]);
                            VisY--;
                            VisX++;
                            if (M.Cord[VisX, VisY] != MapAssets.Wall())
                            {
                                Console.SetCursorPosition(VisX, VisY);
                                Console.Write(M.Cord[VisX, VisY]);
                                VisY--;
                                VisX++;
                                if (M.Cord[VisX, VisY] != MapAssets.Wall())
                                {
                                    Console.SetCursorPosition(VisX, VisY);
                                    Console.Write(M.Cord[VisX, VisY]);
                                    VisY--;
                                    VisX++;
                                    if (M.Cord[VisX, VisY] != MapAssets.Wall())
                                    {
                                        Console.SetCursorPosition(VisX, VisY);
                                        Console.Write(M.Cord[VisX, VisY]);

                                    }
                                }
                            }
                        }
                    }
                }
            }
            #endregion

            #region Clock1
            if (M.Cord[VisX, VisY] != MapAssets.Wall())
            {
                VisY--;
                VisX++;
                if (M.Cord[VisX, VisY] != MapAssets.Wall())
                {
                    Console.SetCursorPosition(VisX, VisY);
                    Console.Write(M.Cord[VisX, VisY]);
                    VisY--;
                    VisX++;
                    if (M.Cord[VisX, VisY] != MapAssets.Wall())
                    {
                        Console.SetCursorPosition(VisX, VisY);
                        Console.Write(M.Cord[VisX, VisY]);
                        VisY--;
                        VisX++;
                        if (M.Cord[VisX, VisY] != MapAssets.Wall())
                        {
                            Console.SetCursorPosition(VisX, VisY);
                            Console.Write(M.Cord[VisX, VisY]);
                            VisY--;
                            VisX++;
                            if (M.Cord[VisX, VisY] != MapAssets.Wall())
                            {
                                Console.SetCursorPosition(VisX, VisY);
                                Console.Write(M.Cord[VisX, VisY]);
                                VisY--;
                                VisX++;
                                if (M.Cord[VisX, VisY] != MapAssets.Wall())
                                {
                                    Console.SetCursorPosition(VisX, VisY);
                                    Console.Write(M.Cord[VisX, VisY]);
                                    VisY--;
                                    VisX++;
                                    if (M.Cord[VisX, VisY] != MapAssets.Wall())
                                    {
                                        Console.SetCursorPosition(VisX, VisY);
                                        Console.Write(M.Cord[VisX, VisY]);

                                    }
                                }
                            }
                        }
                    }
                }
            }
            #endregion
        }

        public string MapCoord(Map M)
        {
            string m = M.CurrentMap;
            //M.cord = new char[152, 36]; <--Old code
            MapAssets.MapsCord(M);
            int[] x = new int[M.cord.GetLength(0)];
            int[] y = new int[M.cord.GetLength(1)];

            var stdout = GetStdHandle(-11);
            for (uint i = 0; i < y.Length; i++)
            {
                for (uint j = 0; j < x.Length; j++)
                {
                    uint coord = j;        // loword  <-- X coord to read
                    coord |= i << 16;           // hiword  <-- Y coord to read

                    if (!ReadConsoleOutputCharacterA(
                            stdout,
                            out byte chAnsi,    // result: single ANSI char
                            1,                  // # of chars to read
                            coord,              // (X,Y) screen location to read (see above)
                            out _))             // result: actual # of chars (unwanted)
                    {
                        throw new Win32Exception();
                    }

                    if (j == (x.Length - 1))
                    {
                        Console.WriteLine();
                    }
                    else
                    {
                        Console.Write((Char)chAnsi);
                    }
                    M.cord[j, i] = (Char)chAnsi;

                }
            }
            Console.ReadKey();
            return  m; 
        }

        /*public bool Walkable(ConsoleKey command) <--Needs implementing
        {
            switch (switch_on)
            {
                default:
            }
        }
        */

        //Code below is just to test ReadConsoleOutputCharacter function, is'nt part of the rest of code
        #region Tekniikka testi 
        private void Go()
        {
            Populate_Console();
            Windows_Console_Readback();
            Console.ReadKey();
        }

        void Populate_Console()
        {
            Console.Clear();
            Console.Write(@"
 ┌───────┐
1│C D E F│
2│G H I J│
3│K L M N│
4│O P Q R│
 └───────┘
  2 4 6 8

    ".TrimStart('\r', '\n'));
        }

        static void Windows_Console_Readback()
        {
            var stdout = GetStdHandle(-11);

            for (uint coord, y = 1; y <= 4; y++)
            {
                coord = (5 - y) * 2;        // loword  <-- X coord to read
                coord |= y << 16;           // hiword  <-- Y coord to read

                if (!ReadConsoleOutputCharacterA(
                        stdout,
                        out byte chAnsi,    // result: single ANSI char
                        1,                  // # of chars to read
                        coord,              // (X,Y) screen location to read (see above)
                        out _))             // result: actual # of chars (unwanted)
                    throw new Win32Exception();

                Console.Write(" " + (Char)chAnsi + " ");
            }
        }
        #endregion
  
    }
}

