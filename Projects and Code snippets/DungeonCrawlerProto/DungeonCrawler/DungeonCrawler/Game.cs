﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonCrawler
{
    public class Game
    {
        Map M;
        Visibility Vis;
        private bool playing = false;
        //private decimal gameSpeed = 100; <-- Left in, if wanna do "real time" game later date


        public void Start()
        {
            M = new Map();
            Vis = new Visibility();
            M.MapInit(M);
            string yo = M.CurrentMap1;
            Console.WriteLine(yo);//Just checking that map printed right
            Console.ReadKey();
            Console.Clear();
            M.DrawMap(M);
        }

        public void MainLoop()
        {
            Console.CursorVisible = false;
            bool keyPress = true;
            int d = 1;
            //M.DrawMap(M);
            do
            {
                /*if (Console.KeyAvailable)
                {
                    Console.ReadKey(true);
                }*/
                //Console.ReadKey(true); <--- tekee pelistä tökkivän, mutta pysäyttää loopin: onko tarvetta?
                ConsoleKey command = Console.ReadKey(true).Key;
                Debug.WriteLine("Loop "+d);
                d++;
                if (playing)
                {
                    Console.Clear();
                }
                #region Movement
                switch (command)
                {
                    case ConsoleKey.NumPad1:
                        Console.SetCursorPosition(M.playerX1, M.playerY1);
                        Console.Write(M.Cord[M.playerX1, M.playerY1]);
                        M.playerX1--;
                        M.playerY1++;
                        Console.SetCursorPosition(M.playerX1, M.playerY1);
                        if (M.Cord[M.playerX1, M.playerY1]==MapAssets.Wall())
                        {
                            M.playerX1++;
                            M.playerY1--;
                            Console.SetCursorPosition(M.playerX1, M.playerY1);
                            Console.Write(MapAssets.PlayerIcon());
                        }
                        else
                        {
                            Console.SetCursorPosition(M.playerX1, M.playerY1);
                            Console.Write(MapAssets.PlayerIcon());
                        }
                        break;

                    case ConsoleKey.NumPad2:
                        Console.SetCursorPosition(M.playerX1, M.playerY1);
                        Console.Write(M.Cord[M.playerX1, M.playerY1]);
                        M.playerY1++;
                        if (M.Cord[M.playerX1, M.playerY1] == MapAssets.Wall())
                        {
                            M.playerY1--;
                            Console.SetCursorPosition(M.playerX1, M.playerY1);
                            Console.Write(MapAssets.PlayerIcon());
                        }
                        else
                        {
                            Console.SetCursorPosition(M.playerX1, M.playerY1);
                            Console.Write(MapAssets.PlayerIcon());
                        }
                        break;

                    case ConsoleKey.NumPad3:
                        Console.SetCursorPosition(M.playerX1, M.playerY1);
                        Console.Write(M.Cord[M.playerX1, M.playerY1]);
                        M.playerX1++;
                        M.playerY1++;
                        if (M.Cord[M.playerX1, M.playerY1] == MapAssets.Wall())
                        {
                            M.playerX1--;
                            M.playerY1--;
                            Console.SetCursorPosition(M.playerX1, M.playerY1);
                            Console.Write(MapAssets.PlayerIcon());
                        }
                        else
                        {
                            Console.SetCursorPosition(M.playerX1, M.playerY1);
                            Console.Write(MapAssets.PlayerIcon());
                        }
                        break;

                    case ConsoleKey.NumPad4:
                        Console.SetCursorPosition(M.playerX1, M.playerY1);
                        Console.Write(M.Cord[M.playerX1, M.playerY1]);
                        M.playerX1--;
                        if (M.Cord[M.playerX1, M.playerY1] == MapAssets.Wall())
                        {
                            M.playerX1++;
                            Console.SetCursorPosition(M.playerX1, M.playerY1);
                            Console.Write(MapAssets.PlayerIcon());
                        }
                        else
                        {
                            Console.SetCursorPosition(M.playerX1, M.playerY1);
                            Console.Write(MapAssets.PlayerIcon());
                        }
                        break;

                    case ConsoleKey.NumPad6:
                        Console.SetCursorPosition(M.playerX1, M.playerY1);
                        Console.Write(M.Cord[M.playerX1, M.playerY1]);
                        M.playerX1++;
                        if (M.Cord[M.playerX1, M.playerY1] == MapAssets.Wall())
                        {
                            M.playerX1--;
                            Console.SetCursorPosition(M.playerX1, M.playerY1);
                            Console.Write(MapAssets.PlayerIcon());
                        }
                        else
                        {
                            Console.SetCursorPosition(M.playerX1, M.playerY1);
                            Console.Write(MapAssets.PlayerIcon());
                        }
                        break;

                    case ConsoleKey.NumPad7:
                        Console.SetCursorPosition(M.playerX1, M.playerY1);
                        Console.Write(M.Cord[M.playerX1, M.playerY1]);
                        M.playerX1--;
                        M.playerY1--;
                        if (M.Cord[M.playerX1, M.playerY1] == MapAssets.Wall())
                        {
                            M.playerX1++;
                            M.playerY1++;
                            Console.SetCursorPosition(M.playerX1, M.playerY1);
                            Console.Write(MapAssets.PlayerIcon());
                        }
                        else
                        {
                            Console.SetCursorPosition(M.playerX1, M.playerY1);
                            Console.Write(MapAssets.PlayerIcon());
                        }
                        break;

                    case ConsoleKey.NumPad8:
                        Console.SetCursorPosition(M.playerX1, M.playerY1);
                        Console.Write(M.Cord[M.playerX1, M.playerY1]);
                        M.playerY1--;
                        if (M.Cord[M.playerX1, M.playerY1] == MapAssets.Wall())
                        {
                            M.playerY1++;
                            Console.SetCursorPosition(M.playerX1, M.playerY1);
                            Console.Write(MapAssets.PlayerIcon());
                        }
                        else
                        {
                            Console.SetCursorPosition(M.playerX1, M.playerY1);
                            Console.Write(MapAssets.PlayerIcon());
                        }
                        break;

                    case ConsoleKey.NumPad9:
                        Console.SetCursorPosition(M.playerX1, M.playerY1);
                        Console.Write(M.Cord[M.playerX1, M.playerY1]);
                        M.playerX1++;
                        M.playerY1--;
                        if (M.Cord[M.playerX1, M.playerY1] == MapAssets.Wall())
                        {
                            M.playerX1--;
                            M.playerY1++;
                            Console.SetCursorPosition(M.playerX1, M.playerY1);
                            Console.Write(MapAssets.PlayerIcon());
                        }
                        else
                        {
                            Console.SetCursorPosition(M.playerX1, M.playerY1);
                            Console.Write(MapAssets.PlayerIcon());
                        }
                        break;
                    default:
                        break;       
                }
                #endregion
                playing = true;
                //M.DrawMap(M);
                Vis.PlayerVisibility(ref M);

                /*if (Console.KeyAvailable) command = Console.ReadKey(true).Key; <-- Part of "real time" code 
                {
                    //Slow game down
                    System.Threading.Thread.Sleep(Convert.ToInt32(gameSpeed));
                }*/
            } while (true);
            
        }
        
    }
}
