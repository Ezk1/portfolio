﻿using System;

namespace DungeonCrawler
{
    class Visibility
    {
        public void PlayerVisibility(ref Map M)
        {
            VisibilityN(ref M);
            VisibilityNE(ref M);
            VisibilityE(ref M);
            VisibilitySE(ref M);
            VisibilityS(ref M);
            VisibilitySW(ref M);
            VisibilityW(ref M);
            VisibilityNW(ref M);
        }
        #region Squares
        private int s1x, s2x, s3x, s4x, s5x, s6x, s7x, s8x, s9x, s10x, s11x, s12x, s13x, s14x, s15x, s16x, s17x, s18x, s19x;
        private int s1y, s2y, s3y, s4y, s5y, s6y, s7y, s8y, s9y, s10y, s11y, s12y, s13y, s14y, s15y, s16y, s17y, s18y, s19y;
        #endregion
        public void VisibilityN(ref Map M)
        {
            s1x = M.playerX1;
            s1y = M.playerY1;
            s1y--;
            Console.SetCursorPosition(s1x, s1y);
            Console.Write(M.Cord[s1x, s1y]);
            if (MapAssets.Walkable(ref M, s1x, s1y))//2
            {
                s2x = s1x;
                s2y = s1y;
                s2x--;
                s2y--;
                Console.SetCursorPosition(s2x, s2y);
                Console.Write(M.Cord[s2x, s2y]);
                if (MapAssets.Walkable(ref M, s2x, s2y))//5
                {
                    s5x = s2x;
                    s5y = s2y;
                    s5x--;
                    s5y--;
                    Console.SetCursorPosition(s5x, s5y);
                    Console.Write(M.Cord[s5x, s5y]);
                }
                if (MapAssets.Walkable(ref M, s2x, s2y))//6
                {
                    s6x = s2x;
                    s6y = s2y;
                    s6y--;
                    Console.SetCursorPosition(s6x, s6y);
                    Console.Write(M.Cord[s6x, s6y]);
                    if (MapAssets.Walkable(ref M, s6x, s6y))//10
                    {
                        s10x = s6x;
                        s10y = s6y;
                        s10x--;
                        s10y--;
                        Console.SetCursorPosition(s10x, s10y);
                        Console.Write(M.Cord[s10x, s10y]);
                    }
                    if (MapAssets.Walkable(ref M, s6x, s6y))//11
                    {
                        s11x = s6x;
                        s11y = s6y;
                        s11y--;
                        Console.SetCursorPosition(s11x, s11y);
                        Console.Write(M.Cord[s11x, s11y]);
                        if (MapAssets.Walkable(ref M, s11x, s11y))//15
                        {
                            s15x = s11x;
                            s15y = s11y;
                            s15y--;
                            Console.SetCursorPosition(s15x, s15y);
                            Console.Write(M.Cord[s15x, s15y]);
                        }
                    }
                }
            }
            if (MapAssets.Walkable(ref M, s1x, s1y))//3
            {
                s3x = s1x;
                s3y = s1y;
                s3y--;
                Console.SetCursorPosition(s3x, s3y);
                Console.Write(M.Cord[s3x, s3y]);
                if (MapAssets.Walkable(ref M, s3x, s3y))//7
                {
                    s7x = s3x;
                    s7y = s3y;
                    s7y--;
                    Console.SetCursorPosition(s7x, s7y);
                    Console.Write(M.Cord[s7x, s7y]);
                    if (MapAssets.Walkable(ref M, s7x, s7y))//12
                    {
                        s12x = s7x;
                        s12y = s7y;
                        s12y--;
                        Console.SetCursorPosition(s12x, s12y);
                        Console.Write(M.Cord[s12x, s12y]);
                        if (MapAssets.Walkable(ref M, s12x, s12y))//16
                        {
                            s16x = s12x;
                            s16y = s12y;
                            s16y--;
                            Console.SetCursorPosition(s16x, s16y);
                            Console.Write(M.Cord[s16x, s16y]);
                            if (MapAssets.Walkable(ref M, s16x, s16y))//18
                            {
                                s18x = s16x;
                                s18y = s16y;
                                s18y--;
                                Console.SetCursorPosition(s18x, s18y);
                                Console.Write(M.Cord[s18x, s18y]);
                            }
                        }
                    }
                }
            }
            if (MapAssets.Walkable(ref M, s1x, s1y))//4
            {
                s4x = s1x;
                s4y = s1y;
                s4x++;
                s4y--;
                Console.SetCursorPosition(s4x, s4y);
                Console.Write(M.Cord[s4x, s4y]);
                if (MapAssets.Walkable(ref M, s4x, s4y))//8
                {
                    s8x = s4x;
                    s8y = s4y;
                    s8y--;
                    Console.SetCursorPosition(s8x, s8y);
                    Console.Write(M.Cord[s8x, s8y]);
                    if (MapAssets.Walkable(ref M, s8x, s8y))//13
                    {
                        s13x = s8x;
                        s13y = s8y;
                        s13y--;
                        Console.SetCursorPosition(s13x, s13y);
                        Console.Write(M.Cord[s13x, s13y]);
                        if (MapAssets.Walkable(ref M, s13x, s13y))//17
                        {
                            s17x = s13x;
                            s17y = s13y;
                            s17y--;
                            Console.SetCursorPosition(s17x, s17y);
                            Console.Write(M.Cord[s17x, s17y]);
                        }
                    }
                    if (MapAssets.Walkable(ref M, s8x, s8y))//14
                    {
                        s14x = s8x;
                        s14y = s8y;
                        s14x++;
                        s14y--;
                        Console.SetCursorPosition(s14x, s14y);
                        Console.Write(M.Cord[s14x, s14y]);
                    }
                }
                if (MapAssets.Walkable(ref M, s4x, s4y))//9
                {
                    s9x = s4x;
                    s9y = s4y;
                    s9x++;
                    s9y--;
                    Console.SetCursorPosition(s9x, s9y);
                    Console.Write(M.Cord[s9x, s9y]);
                    if (MapAssets.Walkable(ref M, s9x, s9y))//14
                    {
                        s14x = s9x;
                        s14y = s9y;
                        s14y--;
                        Console.SetCursorPosition(s14x, s14y);
                        Console.Write(M.Cord[s14x, s14y]);
                    }
                }
            }
        }//Done
        public void VisibilityNE(ref Map M)
        {
            s1x = M.playerX1;
            s1y = M.playerY1;
            s1x++;
            s1y--;
            Console.SetCursorPosition(s1x, s1y);
            Console.Write(M.Cord[s1x, s1y]);
            if (MapAssets.Walkable(ref M, s1x, s1y))//2
            {
                s2x = s1x;
                s2y = s1y;
                s2y--;
                Console.SetCursorPosition(s2x, s2y);
                Console.Write(M.Cord[s2x, s2y]);
                if (MapAssets.Walkable(ref M, s2x, s2y))//5
                {
                    s5x = s2x;
                    s5y = s2y;
                    s5y--;
                    Console.SetCursorPosition(s5x, s5y);
                    Console.Write(M.Cord[s5x, s5y]);
                    if (MapAssets.Walkable(ref M, s5x, s5y))//10
                    {
                        s10x = s5x;
                        s10y = s5y;
                        s10x++;
                        s10y--;
                        Console.SetCursorPosition(s10x, s10y);
                        Console.Write(M.Cord[s10x, s10y]);
                    }
                }
                if (MapAssets.Walkable(ref M, s2x, s2y))//6
                {
                    s6x = s2x;
                    s6y = s2y;
                    s6x++;
                    s6y--;
                    Console.SetCursorPosition(s6x, s6y);
                    Console.Write(M.Cord[s6x, s6y]);
                    if (MapAssets.Walkable(ref M, s6x, s6y))//10
                    {
                        s10x = s6x;
                        s10y = s6y;
                        s10y--;
                        Console.SetCursorPosition(s10x, s10y);
                        Console.Write(M.Cord[s10x, s10y]);
                    }
                    if (MapAssets.Walkable(ref M, s6x, s6y))//11
                    {
                        s11x = s6x;
                        s11y = s6y;
                        s11x++;
                        s11y--;
                        Console.SetCursorPosition(s11x, s11y);
                        Console.Write(M.Cord[s11x, s11y]);
                        if (MapAssets.Walkable(ref M, s11x, s11y))//15
                        {
                            s15x = s11x;
                            s15y = s11y;
                            s15y--;
                            Console.SetCursorPosition(s15x, s15y);
                            Console.Write(M.Cord[s15x, s15y]);
                        }
                        if (MapAssets.Walkable(ref M, s11x, s11y))//16
                        {
                            s16x = s11x;
                            s16y = s11y;
                            s16x++;
                            s16y--;
                            Console.SetCursorPosition(s16x, s16y);
                            Console.Write(M.Cord[s16x, s16y]);
                        }
                    }
                }
            }
            if (MapAssets.Walkable(ref M, s1x, s1y))//3
            {
                s3x = s1x;
                s3y = s1y;
                s3x++;
                s3y--;
                Console.SetCursorPosition(s3x, s3y);
                Console.Write(M.Cord[s3x, s3y]);
                if (MapAssets.Walkable(ref M, s3x, s3y))//7
                {
                    s7x = s3x;
                    s7y = s3y;
                    s7x++;
                    s7y--;
                    Console.SetCursorPosition(s7x, s7y);
                    Console.Write(M.Cord[s7x, s7y]);
                    if (MapAssets.Walkable(ref M, s7x, s7y))//12
                    {
                        s12x = s7x;
                        s12y = s7y;
                        s12x++;
                        s12y--;
                        Console.SetCursorPosition(s12x, s12y);
                        Console.Write(M.Cord[s12x, s12y]);
                        if (MapAssets.Walkable(ref M, s12x, s12y))//17
                        {
                            s17x = s12x;
                            s17y = s12y;
                            s17x++;
                            s17y--;
                            Console.SetCursorPosition(s17x, s17y);
                            Console.Write(M.Cord[s17x, s17y]);
                        }
                    }
                }
            }
            if (MapAssets.Walkable(ref M, s1x, s1y))//4
            {
                s4x = s1x;
                s4y = s1y;
                s4x++;
                Console.SetCursorPosition(s4x, s4y);
                Console.Write(M.Cord[s4x, s4y]);
                if (MapAssets.Walkable(ref M, s4x, s4y))//8
                {
                    s8x = s4x;
                    s8y = s4y;
                    s8x++;
                    s8y--;
                    Console.SetCursorPosition(s8x, s8y);
                    Console.Write(M.Cord[s8x, s8y]);
                    if (MapAssets.Walkable(ref M, s8x, s8y))//13
                    {
                        s13x = s8x;
                        s13y = s8y;
                        s13x++;
                        s13y--;
                        Console.SetCursorPosition(s13x, s13y);
                        Console.Write(M.Cord[s13x, s13y]);
                        if (MapAssets.Walkable(ref M, s13x, s13y))//18
                        {
                            s18x = s13x;
                            s18y = s13y;
                            s18x++;
                            s18y--;
                            Console.SetCursorPosition(s18x, s18y);
                            Console.Write(M.Cord[s18x, s18y]);
                        }
                        if (MapAssets.Walkable(ref M, s13x, s13y))//19
                        {
                            s19x = s13x;
                            s19y = s13y;
                            s19x++;
                            Console.SetCursorPosition(s19x, s19y);
                            Console.Write(M.Cord[s19x, s19y]);
                        }
                    }
                    if (MapAssets.Walkable(ref M, s8x, s8y))//14
                    {
                        s14x = s8x;
                        s14y = s8y;
                        s14x++;
                        Console.SetCursorPosition(s14x, s14y);
                        Console.Write(M.Cord[s14x, s14y]);
                    }
                }
                if (MapAssets.Walkable(ref M, s4x, s4y))//9
                {
                    s9x = s4x;
                    s9y = s4y;
                    s9x++;
                    Console.SetCursorPosition(s9x, s9y);
                    Console.Write(M.Cord[s9x, s9y]);
                }
            }
        }//Done
        public void VisibilityE(ref Map M)
        {
            s1x = M.playerX1;
            s1y = M.playerY1;
            s1x++;
            Console.SetCursorPosition(s1x, s1y);
            Console.Write(M.Cord[s1x, s1y]);
            if (MapAssets.Walkable(ref M, s1x, s1y))//2
            {
                s2x = s1x;
                s2y = s1y;
                s2x++;
                s2y--;
                Console.SetCursorPosition(s2x, s2y);
                Console.Write(M.Cord[s2x, s2y]);
                if (MapAssets.Walkable(ref M, s2x, s2y))//5
                {
                    s5x = s2x;
                    s5y = s2y;
                    s5x++;
                    s5y--;
                    Console.SetCursorPosition(s5x, s5y);
                    Console.Write(M.Cord[s5x, s5y]);
                }
                if (MapAssets.Walkable(ref M, s2x, s2y))//6
                {
                    s6x = s2x;
                    s6y = s2y;
                    s6x++;
                    Console.SetCursorPosition(s6x, s6y);
                    Console.Write(M.Cord[s6x, s6y]);
                    if (MapAssets.Walkable(ref M, s6x, s6y))//10
                    {
                        s10x = s6x;
                        s10y = s6y;
                        s10x++;
                        s10y--;
                        Console.SetCursorPosition(s10x, s10y);
                        Console.Write(M.Cord[s10x, s10y]);
                    }
                    if (MapAssets.Walkable(ref M, s6x, s6y))//11
                    {
                        s11x = s6x;
                        s11y = s6y;
                        s11x++;
                        Console.SetCursorPosition(s11x, s11y);
                        Console.Write(M.Cord[s11x, s11y]);
                        if (MapAssets.Walkable(ref M, s11x, s11y))//15
                        {
                            s15x = s11x;
                            s15y = s11y;
                            s15x++;
                            Console.SetCursorPosition(s15x, s15y);
                            Console.Write(M.Cord[s15x, s15y]);
                        }
                    }
                }
            }
            if (MapAssets.Walkable(ref M, s1x, s1y))//3
            {
                s3x = s1x;
                s3y = s1y;
                s3x++;
                Console.SetCursorPosition(s3x, s3y);
                Console.Write(M.Cord[s3x, s3y]);
                if (MapAssets.Walkable(ref M, s3x, s3y))//7
                {
                    s7x = s3x;
                    s7y = s3y;
                    s7x++;
                    Console.SetCursorPosition(s7x, s7y);
                    Console.Write(M.Cord[s7x, s7y]);
                    if (MapAssets.Walkable(ref M, s7x, s7y))//12
                    {
                        s12x = s7x;
                        s12y = s7y;
                        s12x++;
                        Console.SetCursorPosition(s12x, s12y);
                        Console.Write(M.Cord[s12x, s12y]);
                        if (MapAssets.Walkable(ref M, s12x, s12y))//16
                        {
                            s16x = s12x;
                            s16y = s12y;
                            s16x++;
                            Console.SetCursorPosition(s16x, s16y);
                            Console.Write(M.Cord[s16x, s16y]);
                            if (MapAssets.Walkable(ref M, s16x, s16y))//18
                            {
                                s18x = s16x;
                                s18y = s16y;
                                s18x++;
                                Console.SetCursorPosition(s18x, s18y);
                                Console.Write(M.Cord[s18x, s18y]);
                            }
                        }
                    }
                }
            }
            if (MapAssets.Walkable(ref M, s1x, s1y))//4
            {
                s4x = s1x;
                s4y = s1y;
                s4x++;
                s4y++;
                Console.SetCursorPosition(s4x, s4y);
                Console.Write(M.Cord[s4x, s4y]);
                if (MapAssets.Walkable(ref M, s4x, s4y))//8
                {
                    s8x = s4x;
                    s8y = s4y;
                    s8x++;
                    Console.SetCursorPosition(s8x, s8y);
                    Console.Write(M.Cord[s8x, s8y]);
                    if (MapAssets.Walkable(ref M, s8x, s8y))//13
                    {
                        s13x = s8x;
                        s13y = s8y;
                        s13x++;
                        Console.SetCursorPosition(s13x, s13y);
                        Console.Write(M.Cord[s13x, s13y]);
                        if (MapAssets.Walkable(ref M, s13x, s13y))//17
                        {
                            s17x = s13x;
                            s17y = s13y;
                            s17x++;
                            Console.SetCursorPosition(s17x, s17y);
                            Console.Write(M.Cord[s17x, s17y]);
                        }
                    }
                    if (MapAssets.Walkable(ref M, s8x, s8y))//14
                    {
                        s14x = s8x;
                        s14y = s8y;
                        s14x++;
                        s14y++;
                        Console.SetCursorPosition(s14x, s14y);
                        Console.Write(M.Cord[s14x, s14y]);
                    }
                }
                if (MapAssets.Walkable(ref M, s4x, s4y))//9
                {
                    s9x = s4x;
                    s9y = s4y;
                    s9x++;
                    s9y++;
                    Console.SetCursorPosition(s9x, s9y);
                    Console.Write(M.Cord[s9x, s9y]);
                    if (MapAssets.Walkable(ref M, s9x, s9y))//14
                    {
                        s14x = s9x;
                        s14y = s9y;
                        s14x++;
                        Console.SetCursorPosition(s14x, s14y);
                        Console.Write(M.Cord[s14x, s14y]);
                    }
                }
            }
        }//Done
        public void VisibilitySE(ref Map M)
        {
            s1x = M.playerX1;
            s1y = M.playerY1;
            s1x++;
            s1y++;
            Console.SetCursorPosition(s1x, s1y);
            Console.Write(M.Cord[s1x, s1y]);
            if (MapAssets.Walkable(ref M, s1x, s1y))//2
            {
                s2x = s1x;
                s2y = s1y;
                s2x++;
                Console.SetCursorPosition(s2x, s2y);
                Console.Write(M.Cord[s2x, s2y]);
                if (MapAssets.Walkable(ref M, s2x, s2y))//5
                {
                    s5x = s2x;
                    s5y = s2y;
                    s5x++;
                    Console.SetCursorPosition(s5x, s5y);
                    Console.Write(M.Cord[s5x, s5y]);
                    if (MapAssets.Walkable(ref M, s5x, s5y))//10
                    {
                        s10x = s5x;
                        s10y = s5y;
                        s10x++;
                        s10y++;
                        Console.SetCursorPosition(s10x, s10y);
                        Console.Write(M.Cord[s10x, s10y]);
                    }
                }
                if (MapAssets.Walkable(ref M, s2x, s2y))//6
                {
                    s6x = s2x;
                    s6y = s2y;
                    s6x++;
                    s6y++;
                    Console.SetCursorPosition(s6x, s6y);
                    Console.Write(M.Cord[s6x, s6y]);
                    if (MapAssets.Walkable(ref M, s6x, s6y))//10
                    {
                        s10x = s6x;
                        s10y = s6y;
                        s10x++;
                        Console.SetCursorPosition(s10x, s10y);
                        Console.Write(M.Cord[s10x, s10y]);
                    }
                    if (MapAssets.Walkable(ref M, s6x, s6y))//11
                    {
                        s11x = s6x;
                        s11y = s6y;
                        s11x++;
                        s11y++;
                        Console.SetCursorPosition(s11x, s11y);
                        Console.Write(M.Cord[s11x, s11y]);
                        if (MapAssets.Walkable(ref M, s11x, s11y))//15
                        {
                            s15x = s11x;
                            s15y = s11y;
                            s15x++;
                            Console.SetCursorPosition(s15x, s15y);
                            Console.Write(M.Cord[s15x, s15y]);
                        }
                        if (MapAssets.Walkable(ref M, s11x, s11y))//16
                        {
                            s16x = s11x;
                            s16y = s11y;
                            s16x++;
                            s16y++;
                            Console.SetCursorPosition(s16x, s16y);
                            Console.Write(M.Cord[s16x, s16y]);
                        }
                    }
                }
            }
            if (MapAssets.Walkable(ref M, s1x, s1y))//3
            {
                s3x = s1x;
                s3y = s1y;
                s3x++;
                s3y++;
                Console.SetCursorPosition(s3x, s3y);
                Console.Write(M.Cord[s3x, s3y]);
                if (MapAssets.Walkable(ref M, s3x, s3y))//7
                {
                    s7x = s3x;
                    s7y = s3y;
                    s7x++;
                    s7y++;
                    Console.SetCursorPosition(s7x, s7y);
                    Console.Write(M.Cord[s7x, s7y]);
                    if (MapAssets.Walkable(ref M, s7x, s7y))//12
                    {
                        s12x = s7x;
                        s12y = s7y;
                        s12x++;
                        s12y++;
                        Console.SetCursorPosition(s12x, s12y);
                        Console.Write(M.Cord[s12x, s12y]);
                        if (MapAssets.Walkable(ref M, s12x, s12y))//17
                        {
                            s17x = s12x;
                            s17y = s12y;
                            s17x++;
                            s17y++;
                            Console.SetCursorPosition(s17x, s17y);
                            Console.Write(M.Cord[s17x, s17y]);
                        }
                    }
                }
            }
            if (MapAssets.Walkable(ref M, s1x, s1y))//4
            {
                s4x = s1x;
                s4y = s1y;
                s4x++;
                s4y++;
                Console.SetCursorPosition(s4x, s4y);
                Console.Write(M.Cord[s4x, s4y]);
                if (MapAssets.Walkable(ref M, s4x, s4y))//8
                {
                    s8x = s4x;
                    s8y = s4y;
                    s8x++;
                    s8y++;
                    Console.SetCursorPosition(s8x, s8y);
                    Console.Write(M.Cord[s8x, s8y]);
                    if (MapAssets.Walkable(ref M, s8x, s8y))//13
                    {
                        s13x = s8x;
                        s13y = s8y;
                        s13x++;
                        s13y++;
                        Console.SetCursorPosition(s13x, s13y);
                        Console.Write(M.Cord[s13x, s13y]);
                        if (MapAssets.Walkable(ref M, s13x, s13y))//18
                        {
                            s18x = s13x;
                            s18y = s13y;
                            s18x++;
                            s18y++;
                            Console.SetCursorPosition(s18x, s18y);
                            Console.Write(M.Cord[s18x, s18y]);
                        }
                        if (MapAssets.Walkable(ref M, s13x, s13y))//19
                        {
                            s19x = s13x;
                            s19y = s13y;
                            s19y++;
                            Console.SetCursorPosition(s19x, s19y);
                            Console.Write(M.Cord[s19x, s19y]);
                        }
                    }
                    if (MapAssets.Walkable(ref M, s8x, s8y))//14
                    {
                        s14x = s8x;
                        s14y = s8y;
                        s14y++;
                        Console.SetCursorPosition(s14x, s14y);
                        Console.Write(M.Cord[s14x, s14y]);
                    }
                }
                if (MapAssets.Walkable(ref M, s4x, s4y))//9
                {
                    s9x = s4x;
                    s9y = s4y;
                    s9y++;
                    Console.SetCursorPosition(s9x, s9y);
                    Console.Write(M.Cord[s9x, s9y]);
                }
            }
        }//Done
        public void VisibilityS(ref Map M)
        {
            s1x = M.playerX1;
            s1y = M.playerY1;
            s1y++;
            Console.SetCursorPosition(s1x, s1y);
            Console.Write(M.Cord[s1x, s1y]);
            if (MapAssets.Walkable(ref M, s1x, s1y))//2
            {
                s2x = s1x;
                s2y = s1y;
                s2x--;
                s2y++;
                Console.SetCursorPosition(s2x, s2y);
                Console.Write(M.Cord[s2x, s2y]);
                if (MapAssets.Walkable(ref M, s2x, s2y))//5
                {
                    s5x = s2x;
                    s5y = s2y;
                    s5x--;
                    s5y++;
                    Console.SetCursorPosition(s5x, s5y);
                    Console.Write(M.Cord[s5x, s5y]);
                }
                if (MapAssets.Walkable(ref M, s2x, s2y))//6
                {
                    s6x = s2x;
                    s6y = s2y;
                    s6y++;
                    Console.SetCursorPosition(s6x, s6y);
                    Console.Write(M.Cord[s6x, s6y]);
                    if (MapAssets.Walkable(ref M, s6x, s6y))//10
                    {
                        s10x = s6x;
                        s10y = s6y;
                        s10x--;
                        s10y++;
                        Console.SetCursorPosition(s10x, s10y);
                        Console.Write(M.Cord[s10x, s10y]);
                    }
                    if (MapAssets.Walkable(ref M, s6x, s6y))//11
                    {
                        s11x = s6x;
                        s11y = s6y;
                        s11y++;
                        Console.SetCursorPosition(s11x, s11y);
                        Console.Write(M.Cord[s11x, s11y]);
                        if (MapAssets.Walkable(ref M, s11x, s11y))//15
                        {
                            s15x = s11x;
                            s15y = s11y;
                            s15y++;
                            Console.SetCursorPosition(s15x, s15y);
                            Console.Write(M.Cord[s15x, s15y]);
                        }
                    }
                }
            }
            if (MapAssets.Walkable(ref M, s1x, s1y))//3
            {
                s3x = s1x;
                s3y = s1y;
                s3y++;
                Console.SetCursorPosition(s3x, s3y);
                Console.Write(M.Cord[s3x, s3y]);
                if (MapAssets.Walkable(ref M, s3x, s3y))//7
                {
                    s7x = s3x;
                    s7y = s3y;
                    s7y++;
                    Console.SetCursorPosition(s7x, s7y);
                    Console.Write(M.Cord[s7x, s7y]);
                    if (MapAssets.Walkable(ref M, s7x, s7y))//12
                    {
                        s12x = s7x;
                        s12y = s7y;
                        s12y++;
                        Console.SetCursorPosition(s12x, s12y);
                        Console.Write(M.Cord[s12x, s12y]);
                        if (MapAssets.Walkable(ref M, s12x, s12y))//16
                        {
                            s16x = s12x;
                            s16y = s12y;
                            s16y++;
                            Console.SetCursorPosition(s16x, s16y);
                            Console.Write(M.Cord[s16x, s16y]);
                            if (MapAssets.Walkable(ref M, s16x, s16y))//18
                            {
                                s18x = s16x;
                                s18y = s16y;
                                s18y++;
                                Console.SetCursorPosition(s18x, s18y);
                                Console.Write(M.Cord[s18x, s18y]);
                            }
                        }
                    }
                }
            }
            if (MapAssets.Walkable(ref M, s1x, s1y))//4
            {
                s4x = s1x;
                s4y = s1y;
                s4x++;
                s4y++;
                Console.SetCursorPosition(s4x, s4y);
                Console.Write(M.Cord[s4x, s4y]);
                if (MapAssets.Walkable(ref M, s4x, s4y))//8
                {
                    s8x = s4x;
                    s8y = s4y;
                    s8y++;
                    Console.SetCursorPosition(s8x, s8y);
                    Console.Write(M.Cord[s8x, s8y]);
                    if (MapAssets.Walkable(ref M, s8x, s8y))//13
                    {
                        s13x = s8x;
                        s13y = s8y;
                        s13y++;
                        Console.SetCursorPosition(s13x, s13y);
                        Console.Write(M.Cord[s13x, s13y]);
                        if (MapAssets.Walkable(ref M, s13x, s13y))//17
                        {
                            s17x = s13x;
                            s17y = s13y;
                            s17y++;
                            Console.SetCursorPosition(s17x, s17y);
                            Console.Write(M.Cord[s17x, s17y]);
                        }
                    }
                    if (MapAssets.Walkable(ref M, s8x, s8y))//14
                    {
                        s14x = s8x;
                        s14y = s8y;
                        s14x++;
                        s14y++;
                        Console.SetCursorPosition(s14x, s14y);
                        Console.Write(M.Cord[s14x, s14y]);
                    }
                }
                if (MapAssets.Walkable(ref M, s4x, s4y))//9
                {
                    s9x = s4x;
                    s9y = s4y;
                    s9x++;
                    s9y++;
                    Console.SetCursorPosition(s9x, s9y);
                    Console.Write(M.Cord[s9x, s9y]);
                    if (MapAssets.Walkable(ref M, s9x, s9y))//14
                    {
                        s14x = s9x;
                        s14y = s9y;
                        s14y++;
                        Console.SetCursorPosition(s14x, s14y);
                        Console.Write(M.Cord[s14x, s14y]);
                    }
                }
            }
        }//Done
        public void VisibilitySW(ref Map M)
        {
            s1x = M.playerX1;
            s1y = M.playerY1;
            s1x--;
            s1y++;
            Console.SetCursorPosition(s1x, s1y);
            Console.Write(M.Cord[s1x, s1y]);
            if (MapAssets.Walkable(ref M, s1x, s1y))//2
            {
                s2x = s1x;
                s2y = s1y;
                s2x--;
                Console.SetCursorPosition(s2x, s2y);
                Console.Write(M.Cord[s2x, s2y]);
                if (MapAssets.Walkable(ref M, s2x, s2y))//5
                {
                    s5x = s2x;
                    s5y = s2y;
                    s5x--;
                    Console.SetCursorPosition(s5x, s5y);
                    Console.Write(M.Cord[s5x, s5y]);
                    if (MapAssets.Walkable(ref M, s5x, s5y))//10
                    {
                        s10x = s5x;
                        s10y = s5y;
                        s10x--;
                        s10y++;
                        Console.SetCursorPosition(s10x, s10y);
                        Console.Write(M.Cord[s10x, s10y]);
                    }
                }
                if (MapAssets.Walkable(ref M, s2x, s2y))//6
                {
                    s6x = s2x;
                    s6y = s2y;
                    s6x--;
                    s6y++;
                    Console.SetCursorPosition(s6x, s6y);
                    Console.Write(M.Cord[s6x, s6y]);
                    if (MapAssets.Walkable(ref M, s6x, s6y))//10
                    {
                        s10x = s6x;
                        s10y = s6y;
                        s10x--;
                        Console.SetCursorPosition(s10x, s10y);
                        Console.Write(M.Cord[s10x, s10y]);
                    }
                    if (MapAssets.Walkable(ref M, s6x, s6y))//11
                    {
                        s11x = s6x;
                        s11y = s6y;
                        s11x--;
                        s11y++;
                        Console.SetCursorPosition(s11x, s11y);
                        Console.Write(M.Cord[s11x, s11y]);
                        if (MapAssets.Walkable(ref M, s11x, s11y))//15
                        {
                            s15x = s11x;
                            s15y = s11y;
                            s15x--;
                            Console.SetCursorPosition(s15x, s15y);
                            Console.Write(M.Cord[s15x, s15y]);
                        }
                        if (MapAssets.Walkable(ref M, s11x, s11y))//16
                        {
                            s16x = s11x;
                            s16y = s11y;
                            s16x--;
                            s16y++;
                            Console.SetCursorPosition(s16x, s16y);
                            Console.Write(M.Cord[s16x, s16y]);
                        }
                    }
                }
            }
            if (MapAssets.Walkable(ref M, s1x, s1y))//3
            {
                s3x = s1x;
                s3y = s1y;
                s3x--;
                s3y++;
                Console.SetCursorPosition(s3x, s3y);
                Console.Write(M.Cord[s3x, s3y]);
                if (MapAssets.Walkable(ref M, s3x, s3y))//7
                {
                    s7x = s3x;
                    s7y = s3y;
                    s7x--;
                    s7y++;
                    Console.SetCursorPosition(s7x, s7y);
                    Console.Write(M.Cord[s7x, s7y]);
                    if (MapAssets.Walkable(ref M, s7x, s7y))//12
                    {
                        s12x = s7x;
                        s12y = s7y;
                        s12x--;
                        s12y++;
                        Console.SetCursorPosition(s12x, s12y);
                        Console.Write(M.Cord[s12x, s12y]);
                        if (MapAssets.Walkable(ref M, s12x, s12y))//17
                        {
                            s17x = s12x;
                            s17y = s12y;
                            s17x--;
                            s17y++;
                            Console.SetCursorPosition(s17x, s17y);
                            Console.Write(M.Cord[s17x, s17y]);
                        }
                    }
                }
            }
            if (MapAssets.Walkable(ref M, s1x, s1y))//4
            {
                s4x = s1x;
                s4y = s1y;
                s4x--;
                s4y++;
                Console.SetCursorPosition(s4x, s4y);
                Console.Write(M.Cord[s4x, s4y]);
                if (MapAssets.Walkable(ref M, s4x, s4y))//8
                {
                    s8x = s4x;
                    s8y = s4y;
                    s8x--;
                    s8y++;
                    Console.SetCursorPosition(s8x, s8y);
                    Console.Write(M.Cord[s8x, s8y]);
                    if (MapAssets.Walkable(ref M, s8x, s8y))//13
                    {
                        s13x = s8x;
                        s13y = s8y;
                        s13x--;
                        s13y++;
                        Console.SetCursorPosition(s13x, s13y);
                        Console.Write(M.Cord[s13x, s13y]);
                        if (MapAssets.Walkable(ref M, s13x, s13y))//18
                        {
                            s18x = s13x;
                            s18y = s13y;
                            s18x--;
                            s18y++;
                            Console.SetCursorPosition(s18x, s18y);
                            Console.Write(M.Cord[s18x, s18y]);
                        }
                        if (MapAssets.Walkable(ref M, s13x, s13y))//19
                        {
                            s19x = s13x;
                            s19y = s13y;
                            s19y++;
                            Console.SetCursorPosition(s19x, s19y);
                            Console.Write(M.Cord[s19x, s19y]);
                        }
                    }
                    if (MapAssets.Walkable(ref M, s8x, s8y))//14
                    {
                        s14x = s8x;
                        s14y = s8y;
                        s14y++;
                        Console.SetCursorPosition(s14x, s14y);
                        Console.Write(M.Cord[s14x, s14y]);
                    }
                }
                if (MapAssets.Walkable(ref M, s4x, s4y))//9
                {
                    s9x = s4x;
                    s9y = s4y;
                    s9y++;
                    Console.SetCursorPosition(s9x, s9y);
                    Console.Write(M.Cord[s9x, s9y]);
                }
            }
        }//Done
        public void VisibilityW(ref Map M)
        {
            s1x = M.playerX1;
            s1y = M.playerY1;
            s1x--;
            Console.SetCursorPosition(s1x, s1y);
            Console.Write(M.Cord[s1x, s1y]);
            if (MapAssets.Walkable(ref M, s1x, s1y))//2
            {
                s2x = s1x;
                s2y = s1y;
                s2x--;
                s2y--;
                Console.SetCursorPosition(s2x, s2y);
                Console.Write(M.Cord[s2x, s2y]);
                if (MapAssets.Walkable(ref M, s2x, s2y))//5
                {
                    s5x = s2x;
                    s5y = s2y;
                    s5x--;
                    s5y--;
                    Console.SetCursorPosition(s5x, s5y);
                    Console.Write(M.Cord[s5x, s5y]);
                }
                if (MapAssets.Walkable(ref M, s2x, s2y))//6
                {
                    s6x = s2x;
                    s6y = s2y;
                    s6x--;
                    Console.SetCursorPosition(s6x, s6y);
                    Console.Write(M.Cord[s6x, s6y]);
                    if (MapAssets.Walkable(ref M, s6x, s6y))//10
                    {
                        s10x = s6x;
                        s10y = s6y;
                        s10x--;
                        s10y--;
                        Console.SetCursorPosition(s10x, s10y);
                        Console.Write(M.Cord[s10x, s10y]);
                    }
                    if (MapAssets.Walkable(ref M, s6x, s6y))//11
                    {
                        s11x = s6x;
                        s11y = s6y;
                        s11x--;
                        Console.SetCursorPosition(s11x, s11y);
                        Console.Write(M.Cord[s11x, s11y]);
                        if (MapAssets.Walkable(ref M, s11x, s11y))//15
                        {
                            s15x = s11x;
                            s15y = s11y;
                            s15x--;
                            Console.SetCursorPosition(s15x, s15y);
                            Console.Write(M.Cord[s15x, s15y]);
                        }
                    }
                }
            }
            if (MapAssets.Walkable(ref M, s1x, s1y))//3
            {
                s3x = s1x;
                s3y = s1y;
                s3x--;
                Console.SetCursorPosition(s3x, s3y);
                Console.Write(M.Cord[s3x, s3y]);
                if (MapAssets.Walkable(ref M, s3x, s3y))//7
                {
                    s7x = s3x;
                    s7y = s3y;
                    s7x--;
                    Console.SetCursorPosition(s7x, s7y);
                    Console.Write(M.Cord[s7x, s7y]);
                    if (MapAssets.Walkable(ref M, s7x, s7y))//12
                    {
                        s12x = s7x;
                        s12y = s7y;
                        s12x--;
                        Console.SetCursorPosition(s12x, s12y);
                        Console.Write(M.Cord[s12x, s12y]);
                        if (MapAssets.Walkable(ref M, s12x, s12y))//16
                        {
                            s16x = s12x;
                            s16y = s12y;
                            s16x--;
                            Console.SetCursorPosition(s16x, s16y);
                            Console.Write(M.Cord[s16x, s16y]);
                            if (MapAssets.Walkable(ref M, s16x, s16y))//18
                            {
                                s18x = s16x;
                                s18y = s16y;
                                s18x--;
                                Console.SetCursorPosition(s18x, s18y);
                                Console.Write(M.Cord[s18x, s18y]);
                            }
                        }
                    }
                }
            }
            if (MapAssets.Walkable(ref M, s1x, s1y))//4
            {
                s4x = s1x;
                s4y = s1y;
                s4x--;
                s4y++;
                Console.SetCursorPosition(s4x, s4y);
                Console.Write(M.Cord[s4x, s4y]);
                if (MapAssets.Walkable(ref M, s4x, s4y))//8
                {
                    s8x = s4x;
                    s8y = s4y;
                    s8x--;
                    Console.SetCursorPosition(s8x, s8y);
                    Console.Write(M.Cord[s8x, s8y]);
                    if (MapAssets.Walkable(ref M, s8x, s8y))//13
                    {
                        s13x = s8x;
                        s13y = s8y;
                        s13x--;
                        Console.SetCursorPosition(s13x, s13y);
                        Console.Write(M.Cord[s13x, s13y]);
                        if (MapAssets.Walkable(ref M, s13x, s13y))//17
                        {
                            s17x = s13x;
                            s17y = s13y;
                            s17x--;
                            Console.SetCursorPosition(s17x, s17y);
                            Console.Write(M.Cord[s17x, s17y]);
                        }
                    }
                    if (MapAssets.Walkable(ref M, s8x, s8y))//14
                    {
                        s14x = s8x;
                        s14y = s8y;
                        s14x--;
                        s14y++;
                        Console.SetCursorPosition(s14x, s14y);
                        Console.Write(M.Cord[s14x, s14y]);
                    }
                }
                if (MapAssets.Walkable(ref M, s4x, s4y))//9
                {
                    s9x = s4x;
                    s9y = s4y;
                    s9x--;
                    s9y++;
                    Console.SetCursorPosition(s9x, s9y);
                    Console.Write(M.Cord[s9x, s9y]);
                    if (MapAssets.Walkable(ref M, s9x, s9y))//14
                    {
                        s14x = s9x;
                        s14y = s9y;
                        s14x--;
                        Console.SetCursorPosition(s14x, s14y);
                        Console.Write(M.Cord[s14x, s14y]);
                    }
                }
            }
        }//Done
        public void VisibilityNW(ref Map M)
        {
            s1x = M.playerX1;
            s1y = M.playerY1;
            s1x--;
            s1y--;
            Console.SetCursorPosition(s1x, s1y);
            Console.Write(M.Cord[s1x, s1y]);
            if (MapAssets.Walkable(ref M, s1x, s1y))//2
            {
                s2x = s1x;
                s2y = s1y;
                s2y--;
                Console.SetCursorPosition(s2x, s2y);
                Console.Write(M.Cord[s2x, s2y]);
                if (MapAssets.Walkable(ref M, s2x, s2y))//5
                {
                    s5x = s2x;
                    s5y = s2y;
                    s5y--;
                    Console.SetCursorPosition(s5x, s5y);
                    Console.Write(M.Cord[s5x, s5y]);
                    if (MapAssets.Walkable(ref M, s5x, s5y))//10
                    {
                        s10x = s5x;
                        s10y = s5y;
                        s10x--;
                        s10y--;
                        Console.SetCursorPosition(s10x, s10y);
                        Console.Write(M.Cord[s10x, s10y]);
                    }
                }
                if (MapAssets.Walkable(ref M, s2x, s2y))//6
                {
                    s6x = s2x;
                    s6y = s2y;
                    s6x--;
                    s6y--;
                    Console.SetCursorPosition(s6x, s6y);
                    Console.Write(M.Cord[s6x, s6y]);
                    if (MapAssets.Walkable(ref M, s6x, s6y))//10
                    {
                        s10x = s6x;
                        s10y = s6y;
                        s10y--;
                        Console.SetCursorPosition(s10x, s10y);
                        Console.Write(M.Cord[s10x, s10y]);
                    }
                    if (MapAssets.Walkable(ref M, s6x, s6y))//11
                    {
                        s11x = s6x;
                        s11y = s6y;
                        s11x--;
                        s11y--;
                        Console.SetCursorPosition(s11x, s11y);
                        Console.Write(M.Cord[s11x, s11y]);
                        if (MapAssets.Walkable(ref M, s11x, s11y))//15
                        {
                            s15x = s11x;
                            s15y = s11y;
                            s15y--;
                            Console.SetCursorPosition(s15x, s15y);
                            Console.Write(M.Cord[s15x, s15y]);
                        }
                        if (MapAssets.Walkable(ref M, s11x, s11y))//16
                        {
                            s16x = s11x;
                            s16y = s11y;
                            s16x--;
                            s16y--;
                            Console.SetCursorPosition(s16x, s16y);
                            Console.Write(M.Cord[s16x, s16y]);
                        }
                    }
                }
            }
            if (MapAssets.Walkable(ref M, s1x, s1y))//3
            {
                s3x = s1x;
                s3y = s1y;
                s3x--;
                s3y--;
                Console.SetCursorPosition(s3x, s3y);
                Console.Write(M.Cord[s3x, s3y]);
                if (MapAssets.Walkable(ref M, s3x, s3y))//7
                {
                    s7x = s3x;
                    s7y = s3y;
                    s7x--;
                    s7y--;
                    Console.SetCursorPosition(s7x, s7y);
                    Console.Write(M.Cord[s7x, s7y]);
                    if (MapAssets.Walkable(ref M, s7x, s7y))//12
                    {
                        s12x = s7x;
                        s12y = s7y;
                        s12x--;
                        s12y--;
                        Console.SetCursorPosition(s12x, s12y);
                        Console.Write(M.Cord[s12x, s12y]);
                        if (MapAssets.Walkable(ref M, s12x, s12y))//17
                        {
                            s17x = s12x;
                            s17y = s12y;
                            s17x--;
                            s17y--;
                            Console.SetCursorPosition(s17x, s17y);
                            Console.Write(M.Cord[s17x, s17y]);
                        }
                    }
                }
            }
            if (MapAssets.Walkable(ref M, s1x, s1y))//4
            {
                s4x = s1x;
                s4y = s1y;
                s4x--;
                Console.SetCursorPosition(s4x, s4y);
                Console.Write(M.Cord[s4x, s4y]);
                if (MapAssets.Walkable(ref M, s4x, s4y))//8
                {
                    s8x = s4x;
                    s8y = s4y;
                    s8x--;
                    s8y--;
                    Console.SetCursorPosition(s8x, s8y);
                    Console.Write(M.Cord[s8x, s8y]);
                    if (MapAssets.Walkable(ref M, s8x, s8y))//13
                    {
                        s13x = s8x;
                        s13y = s8y;
                        s13x--;
                        s13y--;
                        Console.SetCursorPosition(s13x, s13y);
                        Console.Write(M.Cord[s13x, s13y]);
                        if (MapAssets.Walkable(ref M, s13x, s13y))//18
                        {
                            s18x = s13x;
                            s18y = s13y;
                            s18x--;
                            s18y--;
                            Console.SetCursorPosition(s18x, s18y);
                            Console.Write(M.Cord[s18x, s18y]);
                        }
                        if (MapAssets.Walkable(ref M, s13x, s13y))//19
                        {
                            s19x = s13x;
                            s19y = s13y;
                            s19x--;
                            Console.SetCursorPosition(s19x, s19y);
                            Console.Write(M.Cord[s19x, s19y]);
                        }
                    }
                    if (MapAssets.Walkable(ref M, s8x, s8y))//14
                    {
                        s14x = s8x;
                        s14y = s8y;
                        s14x--;
                        Console.SetCursorPosition(s14x, s14y);
                        Console.Write(M.Cord[s14x, s14y]);
                    }
                }
                if (MapAssets.Walkable(ref M, s4x, s4y))//9
                {
                    s9x = s4x;
                    s9y = s4y;
                    s9x--;
                    Console.SetCursorPosition(s9x, s9y);
                    Console.Write(M.Cord[s9x, s9y]);
                }
            }
        }//Done
    }
}