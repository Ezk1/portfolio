using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/* Dungeon Crawler v 0.1
 * Author: Ville
 */


namespace DungeonCrawler
{
    class Program
    {
        //TODO: FIX MAP HOLES
        //TODO: Make get set for console window
        //TODO: Dynamic map size cord
        //TODO: Make proper walkable check
        //TODO: Make use of character class
        static void Main(string[] args)
        {
            Console.SetWindowSize(Console.LargestWindowWidth,Console.LargestWindowHeight);
            Console.SetBufferSize(300,400);
            Game g = new Game();
            g.Start();
            g.MainLoop();
        }
    }
}
