﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonCrawler
{
    class Character
    {
        private string name;
        private int experiencePoints;

        private int MAXhp;
        private int hp;
        private int armor;

        #region GetSetit
        public string Name { get => name; set => name = value; }
        public int ExperiencePoints { get => experiencePoints; set => experiencePoints = value; }
        public int MAXhp1 { get => MAXhp; set => MAXhp = value; }
        public int Hp { get => hp; set => hp = value; }
        public int Armor { get => armor; set => armor = value; }

        #endregion
    }
}
