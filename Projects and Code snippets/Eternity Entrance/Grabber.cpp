// Fill out your copyright notice in the Description page of Project Settings.

#include "Grabber.h"

// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

}


// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();

	FindPhysicsHandle();
	SetupInputComponent();

}

void UGrabber::SetupInputComponent()
{
	InputComponent = GetOwner()->FindComponentByClass<UInputComponent>();
	if (InputComponent)
	{
		InputComponent->BindAction(TEXT("Grab"), IE_Pressed, this, &UGrabber::Grab);
		InputComponent->BindAction(TEXT("Grab"), IE_Released, this, &UGrabber::Release);
	}
}

void UGrabber::FindPhysicsHandle()
{
	PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
	if (PhysicsHandle)
	{
		// PhysicsHandle is found.
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("No physics handle component found on %s!"), *GetOwner()->GetName());
	}
	
	CurrentHoldingDistance = HoldingDistance;

}

// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	
	////// Reach debug
	//FVector OutPlayerViewPointLocation;
	//FRotator OutPlayerViewPointRotation;

	//GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
	//	OutPlayerViewPointLocation,
	//	OutPlayerViewPointRotation
	//	);

	////Draw a line from player to show reach

	//FVector LineTraceEnd = OutPlayerViewPointLocation + OutPlayerViewPointRotation.Vector() * Reach;

	//DrawDebugLine(
	//	GetWorld(),
	//	OutPlayerViewPointLocation,
	//	LineTraceEnd,
	//	FColor(0, 255, 0),
	//	false,
	//	0.f,
	//	0,
	//	5.f
	//	);
	////// Debug ends

	if (PhysicsHandle->GrabbedComponent)
	{
		PhysicsHandle->SetTargetLocation(GetPlayersHoldingReach());
	}
}

void UGrabber::Grab()
{
	// Try and reach any actor with physics body collision channel set.
	FHitResult HitResult = GetFirstPhysicsBodyInReach();

	// If we hit something then attach the physics handle
	ComponentToGrab = HitResult.GetComponent();

	GrabbedActor = HitResult.GetActor();
	
	if (GrabbedActor)
	{
		if (!PhysicsHandle) { return; }
		
		if (GrabbedActor->ActorHasTag("Openable"))
		{
			CurrentHoldingDistance = HoldingDistance;
			
			PhysicsHandle->GrabComponentAtLocation(
				ComponentToGrab,
				NAME_None,
				GetPlayersReach()
				);
		}
		else
		{
			// These 2 lines is to make things on the wall pickable
			ComponentToGrab->SetMobility(EComponentMobility::Movable); 
			ComponentToGrab->SetSimulatePhysics(true);
			
			CurrentHoldingDistance = HitResult.Distance;
			
			PhysicsHandle->GrabComponentAtLocationWithRotation(
				ComponentToGrab,
				NAME_None,
				HitResult.ImpactPoint,
				GrabbedActor->GetActorRotation()
				);
			PhysicsHandle->GrabbedComponent->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);
		}
	}
}

void UGrabber::Release()
{
	if (!PhysicsHandle) { return; }
	if (PhysicsHandle->GrabbedComponent)
	{
		PhysicsHandle->GrabbedComponent->SetCollisionResponseToChannel(ECC_Pawn, ECR_Block);
	}
	
	PhysicsHandle->ReleaseComponent();
	GrabbedActor = nullptr;
}

FHitResult UGrabber::GetFirstPhysicsBodyInReach() const
{
	FHitResult OutHit;
	FCollisionQueryParams TraceParams(FName(TEXT("")), false, GetOwner());

	GetWorld()->LineTraceSingleByObjectType(
		OutHit,
		GetPlayersWorldPos(),
		GetPlayersReach(),
		FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody),
		TraceParams
		);

	// See what it hits
	AActor* ActorHit = OutHit.GetActor();

	if (ActorHit)
	{
		/*UE_LOG(LogTemp, Warning, TEXT("Line trace has hit: %s"), *ActorHit->GetName());*/
	}

	return OutHit;
}

FVector UGrabber::GetPlayersReach() const
{
	FVector OutPlayerViewPointLocation;
	FRotator OutPlayerViewPointRotation;

	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
		OutPlayerViewPointLocation,
		OutPlayerViewPointRotation
		);

	return OutPlayerViewPointLocation + OutPlayerViewPointRotation.Vector() * Reach;
}

FVector UGrabber::GetPlayersHoldingReach()
{

	FVector OutPlayerViewPointLocation;
	FRotator OutPlayerViewPointRotation;

	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
		OutPlayerViewPointLocation,
		OutPlayerViewPointRotation
	);
	
	return OutPlayerViewPointLocation + OutPlayerViewPointRotation.Vector() * CurrentHoldingDistance;
}


FVector UGrabber::GetPlayersWorldPos() const
{
	FVector OutPlayerViewPointLocation;
	FRotator OutPlayerViewPointRotation;

	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
		OutPlayerViewPointLocation,
		OutPlayerViewPointRotation
		);

	return OutPlayerViewPointLocation;
}

void UGrabber::RotateObjectX(float &Value)
{

	/*FString loggaus = FString::SanitizeFloat(Value);
	UE_LOG(LogTemp, Warning, TEXT("Arvo: %s"), *loggaus);*/
	//if (!GrabbedActor || GrabbedActor->ActorHasTag("Openable")) { return; }

	////Toimii, mutta py�ritt�� PhysHandle componenttia
	FVector TargetLocation;
	FRotator TargetRotation;
	PhysicsHandle->GetTargetLocationAndRotation(TargetLocation, TargetRotation);
	FRotator NewRotation = TargetRotation + FRotator(0, -Value, 0);
	
	PhysicsHandle->SetTargetRotation(NewRotation);
}

void UGrabber::RotateObjectY(float &Value)
{

	FVector TargetLocation;
	FRotator TargetRotation;
	PhysicsHandle->GetTargetLocationAndRotation(TargetLocation, TargetRotation);
	FRotator NewRotation = TargetRotation + FRotator(Value, 0, 0);

	PhysicsHandle->SetTargetRotation(NewRotation);
}