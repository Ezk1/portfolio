// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCharacter.h"

// Sets default values
APlayerCharacter::APlayerCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	FindGrabber();
	FindMovementComp();
	FindPLayerCamera();

}

void APlayerCharacter::FindGrabber()
{
	GrabberComp = this->FindComponentByClass<UGrabber>();
	if (GrabberComp)
	{
		// Grabber is found.
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("No grabber component found on %s!"), *GetOwner()->GetName());
	}
}

void APlayerCharacter::FindMovementComp()
{
	MovementComp = this->FindComponentByClass<UCharacterMovementComponent>();
	if (MovementComp)
	{
		// MovementComp is found.
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("No character movement component found on %s!"), *GetOwner()->GetName());
	}
}

void APlayerCharacter::FindPLayerCamera()
{
	PlayerCont = GetWorld()->GetFirstPlayerController();
	PlayerCamera = PlayerCont->PlayerCameraManager;
	
	if (PlayerCamera)
	{
		// PlayerCamera is found.
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("No player camera manager found on %s!"), *GetOwner()->GetName());
	}
}

// Called every frame
void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	
}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("Forward"), this, &APlayerCharacter::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("Right"), this, &APlayerCharacter::MoveRight);

	PlayerInputComponent->BindAxis(TEXT("Turn"), this, &APlayerCharacter::TurnCamera);
	PlayerInputComponent->BindAxis(TEXT("LookUp"), this, &APlayerCharacter::LookUpCamera);
	
	PlayerInputComponent->BindAction(TEXT("Running"), IE_Pressed, this, &APlayerCharacter::StartRunning);
	PlayerInputComponent->BindAction(TEXT("Running"), IE_Released, this, &APlayerCharacter::StopRunning);
	
	PlayerInputComponent->BindAction(TEXT("RotatingObject"), IE_Pressed, this, &APlayerCharacter::StartRotating);
	PlayerInputComponent->BindAction(TEXT("RotatingObject"), IE_Released, this, &APlayerCharacter::StopRotating);

	PlayerInputComponent->BindAction(TEXT("Crouch"), IE_Pressed, this, &APlayerCharacter::ToggleCrouch);
	PlayerInputComponent->BindAction(TEXT("Crouch"), IE_Released, this, &APlayerCharacter::ToggleCrouch);

	PlayerInputComponent->BindAction(TEXT("Jump"), IE_Pressed, this, &APlayerCharacter::DoJump);

	PlayerInputComponent->BindAction(TEXT("Zoom"), IE_Pressed, this, &APlayerCharacter::StartZooming);
	PlayerInputComponent->BindAction(TEXT("Zoom"), IE_Released, this, &APlayerCharacter::StopZooming);

}

void APlayerCharacter::MoveForward(float Value)
{
	FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::X);
	AddMovementInput(Direction, Value);
}

void APlayerCharacter::MoveRight(float Value)
{
	FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::Y);
	AddMovementInput(Direction, Value);
}

void APlayerCharacter::TurnCamera(float Value)
{
	if (Value == 0) { return; }

	if (RotatingObject)
	{
		GrabberComp->RotateObjectX(Value);
	}
	AddControllerYawInput(Value);
}

void APlayerCharacter::LookUpCamera(float Value)
{
	if (Value == 0) { return; }

	if (RotatingObject)
	{
		GrabberComp->RotateObjectY(Value);
	}
	AddControllerPitchInput(Value);
}

void APlayerCharacter::StartRunning()
{
	MovementComp->MaxWalkSpeed = RunningSpeed;
}

void APlayerCharacter::StopRunning()
{
	MovementComp->MaxWalkSpeed = WalkingSpeed;
}

void APlayerCharacter::StartRotating()
{
	//UE_LOG(LogTemp, Error, TEXT("click click"));
	if (!GrabberComp->GrabbedActor || GrabberComp->GrabbedActor->ActorHasTag("Openable")) { return; }

	//UE_LOG(LogTemp, Warning, TEXT("Rotatoi!"));
	RotatingObject = true;
	this->Controller->SetIgnoreLookInput(true); // Lock camera when rotating
}

void APlayerCharacter::StopRotating()
{
	//UE_LOG(LogTemp, Error, TEXT("click click"));
	/*if (!GrabberComp->GrabbedActor || GrabberComp->GrabbedActor->ActorHasTag("Openable")) { return; }*/
	
	/*UE_LOG(LogTemp, Warning, TEXT("Ei rotatoi!"));*/
	RotatingObject = false;
	this->Controller->SetIgnoreLookInput(false);
}

void APlayerCharacter::ToggleCrouch()
{
	if (CanCrouch() == true)
	{
		Crouch();
	}
	else
	{
		UnCrouch();
	}
}
void APlayerCharacter::DoJump()
{
	Jump();
}

void APlayerCharacter::StartZooming()
{
	PlayerCamera->SetFOV(ZoomDistance);
}

void APlayerCharacter::StopZooming()
{
	PlayerCamera->UnlockFOV();
}
