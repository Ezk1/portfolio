// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Camera/PlayerCameraManager.h"
#include "Grabber.h"
#include "PlayerCharacter.generated.h"

UCLASS()
class ESCAPELIMBO_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayerCharacter();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY()
		UGrabber* GrabberComp = nullptr;
	UPROPERTY()
		bool RotatingObject = false;
	UPROPERTY()
		APlayerController* PlayerCont = nullptr;

	UPROPERTY()
		UCharacterMovementComponent* MovementComp = nullptr;
	UPROPERTY(EditAnywhere)
		float RunningSpeed = 800;
	UPROPERTY(EditAnywhere)
		float WalkingSpeed = 350;

	UPROPERTY()
		APlayerCameraManager* PlayerCamera = nullptr;
	UPROPERTY(EditAnywhere)
		float ZoomDistance = 60;

	UFUNCTION()
	void MoveForward(float Value);
	UFUNCTION()
	void MoveRight(float Value);
	
	UFUNCTION()
	void TurnCamera(float Value);
	UFUNCTION()
	void LookUpCamera(float Value);

	UFUNCTION()
	void StartRunning();
	UFUNCTION()
	void StopRunning();


	UFUNCTION()
	void ToggleCrouch();
	UFUNCTION()
	void DoJump();

	UFUNCTION()
	void FindGrabber();
	UFUNCTION()
		void FindMovementComp();
	UFUNCTION()
		void FindPLayerCamera();
	
	UFUNCTION()
		void StartRotating();
	UFUNCTION()
		void StopRotating();

	UFUNCTION()
		void StartZooming();
	UFUNCTION()
		void StopZooming();
};
