// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DrawDebugHelpers.h"
#include "Engine/World.h"
#include "Components/ActorComponent.h"
#include "Components/PrimitiveComponent.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "GameFramework/PlayerController.h"
#include "Grabber.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ESCAPELIMBO_API UGrabber : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGrabber();
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY()
	AActor* GrabbedActor = nullptr;
	UFUNCTION()
	void RotateObjectX(float& Value);
	UFUNCTION()
	void RotateObjectY(float& Value);

	UFUNCTION()
		void Release();
	UPROPERTY()
		UPhysicsHandleComponent* PhysicsHandle = nullptr;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;



private:
	UPROPERTY(EditAnywhere)
	float Reach = 150.f;
	UPROPERTY(EditAnywhere)
	float HoldingDistance = 150.f;
	UPROPERTY()
		float CurrentHoldingDistance = 0.f;
	UPROPERTY(EditAnywhere)
		float ReduceDistance = 50.f;
	UPROPERTY(EditAnywhere)
		float LinearDamping = 5000.f;
	UPROPERTY(EditAnywhere)
		float AngularDamping = 5000.f;
	
	UPROPERTY()
	bool ActorRotates = false;

	
	UPROPERTY()
	UInputComponent* InputComponent = nullptr;
	UPROPERTY()
	UPrimitiveComponent* ComponentToGrab = nullptr;
	
	UFUNCTION()
	void Grab();

	UFUNCTION()
	void FindPhysicsHandle();
	UFUNCTION()
	void SetupInputComponent();

	// Return first ACtor in physics body
	UFUNCTION()
	FHitResult GetFirstPhysicsBodyInReach() const;

	//Return LineTraceEnd
	UFUNCTION()
	FVector GetPlayersReach() const;
	UFUNCTION()
	FVector GetPlayersHoldingReach();
	
	// Get players position in world
	UFUNCTION()
	FVector GetPlayersWorldPos() const;

};
